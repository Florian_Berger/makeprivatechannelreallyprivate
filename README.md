# MakePrivateChannelReallyPrivate

Du hast es satt, dass dein MyChannel nicht wirklich privat bleibt, sondern immer irgendwelche unbekannte Chatter durch rumprobieren in den Channel kommen?

Dann ist diese App die Lösung. Mit ihr stellst du sicher, dass (fast) nur freigegebene Personen den Channel betreten dürfen.
Die folgenden Personen können nicht ausgeschlossen werden:
* Channelbesitzer
* Channelmoderatoren & HZM
* Admins (sofern notwendig)
* Sysadmins
* User Apps-Team (Mitarbeiter von Knuddels)

​

#### Installation

Die App kann mit folgendem Befehl installiert werden:  
***/apps install knuddelsDEV.30558468.MakePrivateChannelReallyPrivate***

​

#### Befehle
Die folgenden Befehle stehen in der App zur Verfügung:

 **Befehl** | **Funktion**
-----------------------------------|---------------------------------------------------------
 `/privchannel set:active` | Aktiviert den privaten Modus 
 `/privchannel set:inactive` | Deaktiviert den privaten Modus 
 `/privchannel allow:NICK` | Erlaubt NICK, den Channel zu betreten 
 `/privchannel reject:NICK` | Entzieht NICK das Recht wieder, den Channel zu betreten 

**Free Software, Hell Yeah!**
