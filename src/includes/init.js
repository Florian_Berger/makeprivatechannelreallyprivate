/**
 * @file Initializes all modules and required functions
 *
 * @author Florian Berger
 * @copyright © 2019 Florian Berger
 * @change 2019-03-20
 *
 * @license MIT License
 * @license https://opensource.org/licenses/MIT
 */

// Import module base
require('includes/Module.js');

// Import modules
require('includes/modules/PreventChannelJoin.js');