/**
 * @file Module 'PreventChannelJoin'
 *
 * @author Florian Berger
 * @copyright © 2019 Florian Berger
 * @change 2019-03-20
 *
 * @license MIT License
 * @license https://opensource.org/licenses/MIT
 */

function PreventChannelJoin() {
    App.RegisterModule(this);

    this.registerCommand('privchannel', this.cmdPrivChannel);
}

PreventChannelJoin.prototype = new Module();
PreventChannelJoin.prototype.ModuleName = "PreventChannelJoin";

/**
 *  Will be called when a user types /privchannel
 *
 *  @param user User who typed the command
 *  @param params Entered parameters
 *  @param command The full command
 */
PreventChannelJoin.prototype.cmdPrivChannel = function(user, params, command) {
    if (!user.isChannelOwner() && !user.isAppManager()) {
        App.Private(user, "Sie haben keine Berechtigung, diese Methode auszuführen!");
        return;
    }

    if ((params || '').length < 1) {
        App.Private(user, "Es wurden nicht alle Parameter angegeben!");
        return;
    }

    const tokens = params.split(':');

    switch (tokens[0].toLowerCase()) {
        case 'set':
            if (['1','aktiv','active','yes','an'].indexOf(tokens[1].toLowerCase()) > -1) {
                KnuddelsServer.getPersistence().setNumber('privateModeActive', 1);
                App.Private(user, 'Der private Channelmodus wurde erfolgreich aktiviert.');
                return;
            }

            if (['0','inaktiv','inactive','no','aus'].indexOf(tokens[1].toLowerCase()) > -1) {
                KnuddelsServer.getPersistence().setNumber('privateModeActive', 0);
                App.Private(user, 'Der private Channelmodus wurde erfolgreich deaktiviert.');
                return;
            }
            break;

        case 'allow':
        case 'add':
            const addUserObj = App.GetUser(tokens[1]);
            if (addUserObj) {
                addUserObj.getPersistence().setNumber('mayJoin', 1);
                App.Private(user, 'Dem Benutzer ' + addUserObj.getNick() + ' wurde erlaubt, den Channel zu betreten.');
            } else {
                let currentVal = KnuddelsServer.getPersistence().getString('joinNicks', '');

                if ((currentVal || '').length > 0)
                    currentVal += '|';

                currentVal += tokens[1];

                KnuddelsServer.getPersistence().setString('joinNicks', currentVal);
                App.Private(user, 'Dem Nick ' + tokens[1] + ' wurde erlaubt, den Channel zu betreten.');
            }
            break;

        case 'reject':
        case 'remove':
            const removeUserObj = App.GetUser(tokens[1]);
            if (removeUserObj) {
                removeUserObj.getPersistence().setNumber('mayJoin', 0);
                App.Private(user, 'Dem Benutzer ' + removeUserObj.getNick() + ' wurde die Berechtigung, den Channel zu betreten, entzogen.');
            } else {
                const currentVal = KnuddelsServer.getPersistence().getString('joinNicks', '');
                let nicks = currentVal.split('|');
                nicks = nicks.splice(nicks.indexOf(tokens[1]));

                let newVal = '';
                for (let i = 0; i < nicks.length; i++) {
                    const nick = nicks[i];
                    if ((nick || '').length === 0) continue;

                    if (newVal.length > 0) newVal += '|';
                    newVal += nick;
                }

                KnuddelsServer.getPersistence().setString('joinNicks', newVal);
                App.Private(user, 'Dem Nick ' + tokens[1] + ' wurde die Berechtigung, den Channel zu betreten, entzogen.');
            }
            break;

        default:
            App.Private(user, 'Der angegebene Parameter ist nicht bekannt.');
            break;
    }
};

PreventChannelJoin.prototype.mayJoinChannel = function(user) {
    if (KnuddelsServer.getPersistence().getNumber('privateModeActive', 0) === 0) {
        return ChannelJoinPermission.accepted();
    }

    if (user.getPersistence().getNumber('mayJoin', 0) === 1)
        return ChannelJoinPermission.accepted();

    const unknownUsers = KnuddelsServer.getPersistence().getString('joinNicks', '').toLowerCase().split('|');
    if (unknownUsers.indexOf(user.getNick().toLowerCase()) > -1) {
        user.getPersistence().setNumber('mayJoin', 1);

        unknownUsers.splice(unknownUsers.indexOf(user.getNick().toLowerCase()), 1);
        let newVal = '';
        for (let i = 0; i < unknownUsers.length; i++) {
            const nick = unknownUsers[i];
            if ((nick || '').length === 0) continue;

            if (newVal.length > 0) newVal += '|';
            newVal += nick;
        }

        KnuddelsServer.getPersistence().setString('joinNicks', newVal);

        return ChannelJoinPermission.accepted();
    }

    return ChannelJoinPermission.denied('Der Channel ist aktuell nur für bestimmte User geöffnet.');
};

PreventChannelJoin.self = new PreventChannelJoin();