/**
 * @file Base module file
 *
 * @author Florian Berger
 * @copyright © 2019 Florian Berger
 * @change 2019-03-20
 *
 * @license MIT License
 * @license https://opensource.org/licenses/MIT
 */

function Module() { }

Module.prototype.ModuleName = "";

Module.prototype.register = function() {
    App.RegisterModule(this);
};

Module.prototype.unregister = function() {
    App.UnregisterModule(this);
};

Module.prototype.registerCommand = function (command, func) {
    App.RegisterCommand(command, func.bind(this));
};
Module.prototype.unregisterCommand = function(command) {
    App.UnregisterCommand(command);
};

