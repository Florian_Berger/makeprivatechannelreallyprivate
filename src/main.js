/**
 * @file Main file of the app
 *
 * @author Florian Berger
 * @copyright © 2019 Florian Berger
 * @change 2019-03-20
 *
 * @license MIT License
 * @license https://opensource.org/licenses/MIT
 */

/**
 *  Default app object, holds required data
 */
var App = {};

/**
 *  Array with all registered commands
 */
App.chatCommands = {};


//region Properties extending the App object

/**
 *  Information weather app is starting or not
 */
App.IsStarting = false;

/**
 *  Defines a property for the AppBot in the App object
 */
App.Bot = KnuddelsServer.getDefaultBotUser();

/**
 *  Defines a property for the Logger in the App object
 */
App.Logger = KnuddelsServer.getDefaultLogger();

/**
 *  Defines a property for the Channel in the App object
 */
App.Channel = KnuddelsServer.getChannel();

/**
 *  Defines a property for the UserAccess in the App object
 */
App.UserAccess = KnuddelsServer.getUserAccess();

/**
 *  Defines a property for the ExternalServerAccess in the App object
 */
App.ExternalAccess = KnuddelsServer.getExternalServerAccess();

/**
 * Defines a property for the AppAccess in the App object
 * @type {AppAccess}
 */
App.AppAccess = KnuddelsServer.getAppAccess();

/**
 * Defines a property for the AppInfo in the App object
 * @type {AppInfo}
 */
App.AppInfo = App.AppAccess.getOwnInstance().getAppInfo();

/**
 *  List of all registered modules
 */
App.RegisteredModules = [];

/**
 *  Timeout for hook refresh
 */
App.RefreshHooksTimeout = undefined;

//endregion

//region Functions that simplifies the development

/**
 *  Registers a new module
 *
 *  @param module Module that should be added
 *  @returns {boolean} True if the module was added successfully
 */
App.RegisterModule = function(module) {
    let isRegistered = App.RegisteredModules.indexOf(module) !== -1;
    if (!isRegistered) {
        App.RegisteredModules.push(module);
        App.RefreshHooks();
        return true;
    }

    return false;
};

/**
 *  Unregisters a module
 *
 *  @param module Module that should be removed
 *  @returns {boolean} True if the module was removed successfully
 */
App.UnregisterModule = function(module) {
    var pos = App.RegisteredModules.indexOf(module);
    if (pos !== -1) {
        App.RegisteredModules.splice(pos, 1);
        App.RefreshHooks();
        return true;
    }

    return false;
};

/**
 *  Sends a public message
 *
 *  @param msg Message which should be displayed as a public message
 */
App.Public = function(msg) { App.Bot.sendPublicMessage(msg) };

/**
 *  Sends a action message
 *
 *  @param msg Message which should be displayed as an action
 */
App.Action = function(msg) { App.Bot.sendPublicActionMessage(msg) };

/**
 *  Sends a private message to the defined user
 *
 *  @param user User who should receive the private message
 *  @param msg Text the user sould receive
 */
App.Private = function(user, msg) {
    if (!user) {
        App.Logger.info('App.Private: _user_ is not defined');
        return;
    }

    App.Bot.sendPrivateMessage(msg, user);
};

/**
 *  Sends a post message with the defined topic to the defined user
 *
 *  @param user User who should receive the post
 *  @param topic Topic the message should have
 *  @param msg Message the user should receive
 */
App.Post = function(user, topic, msg) {
    if (!user) {
        App.Logger.info('App.Post: _user_ is not defined');
        return;
    }
    if (!topic) {
        App.Logger.info('App.Post: _topic_ is not defined');
        return;
    }
    if (!msg) {
        App.Logger.info('App.Post: _msg_ is not defined');
        return;
    }

    App.Bot.sendPostMessage(user, topic, msg);
};

/**
 *  Refresh all app hooks
 */
App.RefreshHooks = function() {
    KnuddelsServer.refreshHooks();
    App.RefreshHooksTimeout = undefined;
};

/**
 *  Register a command if not exists
 *  @return {boolean} True if adding succeed
 */
App.RegisterCommand = function(command, executeFunction) {
    if (!command) {
        App.Logger.info('App.RegisterCommands: _command_ is not defined');
        return false;
    }
    if (!executeFunction) {
        App.Logger.info('App.RegisterCommands: _executeFunction_ is not defined');
        return false;
    }

    if (typeof App.chatCommands[command] === 'function') {
        App.Logger.error('Command _' + command + '_ already exists!');
        return false;
    }

    App.chatCommands[command] = executeFunction;
    App.RefreshHooksAfterTime(1000);
    return true;
};

/**
 *  Unregisters a command
 *  @return {boolean} True if removing succeed
 */
App.UnregisterCommand = function(command) {
    if (!command) {
        App.Logger.info('App.RegisterCommands: _command_ is not defined');
        return false;
    }

    if (typeof App.chatCommands[command] !== 'function') {
        App.Logger.error('Command _' + command + '_ not exists!');
        return false;
    }

    delete App.chatCommands[command];
    App.RefreshHooksAfterTime(1000);
    return true;
};

/**
 *  Returns the instance of the module with the defined name.
 *  If no instance exists, returning undefined
 *
 *  @param moduleName Name of the module which should be get
 */
App.GetModuleByName= function(moduleName) {
    if (!App.RegisteredModules) {
        App.Logger.info('GetModuleByName: _getting module ' + moduleName + '"_ failed, RegisteredModules not defined');
        return;
    }

    for(let i = 0; i < App.RegisteredModules.length; i++) {
        if (App.RegisteredModules[i].ModuleName.toLowerCase() === moduleName.toLowerCase())
            return App.RegisteredModules[i];
    }
};

/**
 *  Refresh all app hooks after the given time
 *
 *  @param milliseconds Milliseconds after them the hooks should be refreshed
 */
App.RefreshHooksAfterTime = function(milliseconds) {
    if (!milliseconds)
        milliseconds = 1000;

    if (!App.IsStarting) {
        let msInt = parseInt(milliseconds);

        if (App.RefreshHooksTimeout)
            clearTimeout(App.RefreshHooksTimeout);

        App.RefreshHooksTimeout = setTimeout(App.RefreshHooks, msInt);
    }
};

/**
 *  Returns the user
 *
 *  @param {string} nickName Name des Nutzers, der geladen werden soll
 */
App.GetUser = function(nickName) {
    if (!App.UserAccess.exists(nickName))
        return undefined;

    let userId = App.UserAccess.getUserId(nickName);
    if (App.UserAccess.mayAccess(userId))
        return App.UserAccess.getUserById(userId);

    return undefined;
};

//endregion

//region App events

/**
 *  Is calling when the app is starting
 */
App.onAppStart = function() {
    App.IsStarting = true;
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onAppStart === 'function')
            module.onAppStart();
    }
    App.IsStarting = false;
    App.RefreshHooksAfterTime(500);
};

/**
 *  Is calling before the app shuts down
 *
 * @param time Time to shutdown
 * @param reason Reason while the app is shutting down
 */
App.onPrepareShutdown = function(time, reason) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onPrepareShutdown === 'function')
            module.onPrepareShutdown(time, reason);
    }
};

/**
 *  Is calling when the app shuts down
 */
App.onShutdown = function() {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onShutdown === 'function')
            module.onShutdown();
    }
};

/**
 *  Is calling before the app receives a Knuddel
 *
 *  @param transfer Knuddel transfer object
 */
App.onBeforeKnuddelReceived = function(transfer) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onBeforeKnuddelReceived === 'function') {
            module.onBeforeKnuddelReceived(transfer);
            if (transfer.isProcessed())
                return;
        }
    }
    transfer.accept();
};

/**
 *  Is calling when a user joins the channel
 *
 *  @param user User who joins the channel
 */
App.onUserJoined = function(user) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onUserJoined === 'function')
            module.onUserJoined(user);
    }
};

/**
 *  Is calling when a user left the channel
 *
 *  @param user User who left the channel
 */
App.onUserLeft = function(user) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onUserLeft === 'function')
            module.onUserLeft(user);
    }
};

/**
 *  Is calling when a user send a public message
 *
 *  @param publicMessage Public message object which was sent
 */
App.onPublicMessage = function(publicMessage) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onPublicMessage === 'function')
            module.onPublicMessage(publicMessage);
    }
};

/**
 *  Is calling when the bot receives a private message
 *
 *  @param privateMessage Private message object which was sent
 */
App.onPrivateMessage = function(privateMessage) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onPrivateMessage === 'function')
            module.onPrivateMessage(privateMessage);
    }
};

/**
 *  Is calling when the app receives an event from the HTML UI
 *
 *  @param user User who triggered the event
 *  @param key Key which marks the data
 *  @param data Data which were sent
 */
App.onEventReceived = function(user, key, data) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onEventReceived === 'function')
            module.onEventReceived(user, key, data);
    }
};

App.onKnuddelReceived = function(sender, receiver, knuddelAmount, transferReason) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onKnuddelReceived === 'function')
            module.onKnuddelReceived(sender, receiver, knuddelAmount, transferReason);
    }
};

/**
 *  Is calling when a user diced
 *
 *  @param diceEvent DiceEvent data
 */
App.onUserDiced = function(diceEvent) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onUserDiced === 'function')
            module.onUserDiced(diceEvent);
    }
};

/**
 *  Is calling when a user want to send a public message
 *
 *  @param publicMessage Message which was sent
 */
App.mayShowPublicMessage = function(publicMessage) {
    let allowed = true;
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.mayShowPublicMessage === 'function')
            allowed &= module.mayShowPublicMessage(publicMessage);
    }

    return allowed;
};

/**
 *  Is calling when a user want to send a public action message
 *
 *  @param publicActionMessage ActionMessage which was sent
 */
App.mayShowPublicActionMessage = function(publicActionMessage) {
    let allowed = true;
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.mayShowPublicActionMessage === 'function')
            allowed &= module.mayShowPublicActionMessage(publicActionMessage);
    }

    return allowed;
};

/**
 *  Is calling when a user want to join a channel
 *
 *  @param user Benutzer, der zugreifen möchte
 */
App.mayJoinChannel = function(user) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.mayJoinChannel === 'function') {
            let ret = module.mayJoinChannel(user);
            if (typeof ret != 'undefined')
                return ret;
        }
    }

    return ChannelJoinPermission.accepted();
};

/**
 * Is calling when the count of knuddel is changed on a user account
 *
 * @param user User which KnuddelAccount data has changed
 * @param knuddelAccount Connected KnuddelAccount
 * @param oldKnuddelAmount Old amount of knuddel
 * @param newKnuddelAmount New amount of knuddel
 */
App.onAccountChangedKnuddelAmount = function(user, knuddelAccount, oldKnuddelAmount, newKnuddelAmount) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onAccountChangedKnuddelAmount === 'function') {
            module.onAccountChangedKnuddelAmount(user, knuddelAccount, oldKnuddelAmount, newKnuddelAmount);
        }
    }
};

/**
 * Is called when a user is transferring knuddel to the bot user
 *
 * @param sender Sender of the knuddel
 * @param receiver Bot who should receive the knuddel
 * @param knuddelAmount Number of knuddel which were sent to the bot
 * @param transferReason Reason of the knuddel input
 * @param knuddelAccount KnuddelAccount of the user
 */
App.onAccountReceivedKnuddel = function(sender, receiver, knuddelAmount, transferReason, knuddelAccount) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onAccountReceivedKnuddel === 'function') {
            module.onAccountReceivedKnuddel(sender, receiver, knuddelAmount, transferReason, knuddelAccount);
        }
    }
};

/**
 * Will be called when a event was sent from another app
 *
 * @param appInstance Instance of the app which sent message via 'sendAppEvent'
 * @param type Type which was sent
 * @param data Data which was sent
 */
App.onAppEventReceived = function(appInstance, type, data) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onAppEventReceived === 'function') {
            module.onAppEventReceived(appInstance, type, data);
        }
    }
};

/**
 * Will be called when a public action is executed in the channel of the app.
 *
 * @param publicActionMessage Action message which was executed
 */
App.onPublicActionMessage = function(publicActionMessage) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onPublicActionMessage === 'function') {
            module.onPublicActionMessage(publicActionMessage);
        }
    }
};

/**
 * Will be called when a event message is executed in the channel of the app.
 *
 * @param publicEventMessage Event message which was executed
 */
App.onPublicEventMessage = function(publicEventMessage) {
    for(let i = 0; i < App.RegisteredModules.length; i++) {
        let module = App.RegisteredModules[i];
        if (typeof module.onPublicEventMessage === 'function') {
            module.onPublicEventMessage(publicEventMessage);
        }
    }
};

require("includes/init.js");

//endregion App events